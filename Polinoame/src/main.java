import java.util.*;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
public class main {
	public static void main(String[] args){
		
	    Polinom p1=new Polinom("3x^4+2x^3+5x^2+3x^1+2x^0");
	    System.out.println(p1.transformareInString());
	    System.out.println("asta doi:");
	    Polinom p2=new Polinom("1x^5+3x^3+2x^1");
	    System.out.println(p2.transformareInString());
	    Polinom c=Polinom.adunarePolinoame(p1, p2);
	    Polinom d=Polinom.scaderePolinoame(p1, p2);
	    Polinom e=Polinom.inmultirePolinoame(p1,p2);
	    Polinom f=Polinom.derivarePolinom(p1);
	    Polinom g=Polinom.integrarePolinom(p2);
	    System.out.println("adunarea:");
	    System.out.println(c.transformareInString());
	    System.out.println("scaderea:");
	    System.out.println(d.transformareInString());
	    System.out.println("inmultirea:");
	    System.out.println(e.transformareInString());
	    System.out.println("derivare polinom 1:");
	    System.out.println(f.transformareInString());
	    System.out.println("integrare polinom 2:");
	    System.out.println(g.transformareInString());
	    SwingUtilities.invokeLater(
	    		new Runnable(){
	    			@Override
	    			public void run(){
	    				Grafica g = new Grafica();
	    				JFrame frame = new JFrame();
	    				frame.setVisible(true);
	    				frame.setSize(500,500);
	    				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    				frame.setLocationRelativeTo(null);
	    				frame.add(g);
	    			}
	    		}
	    		);   	        
	    	    
	}
    
}
