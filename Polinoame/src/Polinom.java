import java.util.ArrayList;
public class Polinom{
	private ArrayList<Monom> list= new ArrayList<Monom>();
	
	public Polinom(String poli){
		if(poli != null){
			list = new ArrayList<Monom>();	
			poli=poli.replaceAll("-","+-"); //inlocuim fiecare - cu +- pentru citirea numerelor negative
			poli=poli.replaceAll("\\s","");
			String[] mocod=poli.split("\\+"); //impartim stringul poli intr-un vector de stringuri dupa + care este retinut de split
			for(String a:mocod) // parcurgem vectorul de stringuri
			{
				String[] mocod2=a.split("x");//impartim stringul(un monom) intr-un vector de stringuri dupa x(separam x de coeficient 
										//exponent) retinut de mocod2
				if(mocod2.length==1){
					list.add(new Monom(Integer.parseInt(mocod2[0]),0));
				}
				else {
					mocod2[1]=mocod2[1].replaceAll("\\^", "");
					list.add(new Monom(Integer.parseInt(mocod2[0]),Integer.parseInt(mocod2[1])));
				}
			}
		}
		else
			list = new ArrayList<Monom>();
	}
	public void sumaAdaugareMonom(Monom m){
		for(Monom i:list){
			if(i.getExponent()==m.getExponent()){
				i.add(m);
				return;
		}}
		list.add(m);
	}
	public static Polinom adunarePolinoame(Polinom a,Polinom b) //metoda pentru adunearea polinoamelor
	{
		Polinom suma=new Polinom(null);
		for(Monom m:a.list){ // parcurgem polinomul a
			suma.sumaAdaugareMonom(new Monom(m.getCoeficient(),m.getExponent())); //adaugam toate monoamele din a la polinomul suma
		}
		for(Monom m:b.list){ // parcurgem polinomul b
			suma.sumaAdaugareMonom(new Monom(m.getCoeficient(),m.getExponent())); //adaugam toate monoamele din b la polinomul suma
		}
		return suma;
	}
	public static Polinom scaderePolinoame(Polinom a,Polinom b){ //metoda pentru scaderea polinoamelor
		Polinom suma=new Polinom(null);
		for(Monom m:a.list){
			suma.sumaAdaugareMonom(new Monom(m.getCoeficient(), m.getExponent())); //adaugam toate monoamele din polinomul a la polinomul suma
		}
		for(Monom m:b.list){
			suma.sumaAdaugareMonom(new Monom(-m.getCoeficient(), m.getExponent())); // adaugam toate monoamele din b negate la polinomul suma
		}
		return suma;
	}
	public static Polinom inmultirePolinoame(Polinom a,Polinom b){
		Polinom inmultirea=new Polinom(null);
		for(Monom m:a.list)
			{for(Monom p:b.list)
			{
				inmultirea.sumaAdaugareMonom(Monom.inmultire(m,p));
			}}
		return inmultirea;
	}
	public static Polinom integrarePolinom(Polinom a){
		Polinom b=new Polinom(null);
		for(Monom m:a.list)
		{
			b.sumaAdaugareMonom(new Monom(m.getCoeficient()/m.getExponent(),m.getExponent()+1));}
			return b;
			
	}
	public static Polinom derivarePolinom(Polinom a){
		Polinom b=new Polinom(null);
		for(Monom m:a.list)
		{
			if (m.getExponent() > 0)
				b.sumaAdaugareMonom(new Monom(m.getCoeficient()*m.getExponent(),m.getExponent()-1));
		}
		return b;
			
	}

	
	public String transformareInString(){
		String s = "";
		for(Monom m : list){
			s+=m.transformareInString();
		}
		return s;
	}
	
}
