
public class Monom {
	private int coeficient;
	private int exponent;
	
	public int getCoeficient(){ //getter pentru coeficientul polinomului
		return coeficient;
	}
	public int getExponent(){ //getter pentru exponentul polinomului
		return exponent;
	}
	
	public Monom(int coeficient,int exponent) // constructor pentru instantierea variabilelor
	{
		this.coeficient=coeficient;
		this.exponent=exponent;
	}
	public String transformareInString() // metoda pentru transformarea monomului in string
     {
		if(exponent == 0) return (coeficient >= 0 ? "+" : "")+Integer.toString(coeficient);
        else
        if(exponent == 1) return (coeficient >= 0 ? "+" : "")+coeficient + "x";
        else
        return (coeficient >= 0 ? "+" : "")+coeficient + "x^" + exponent;
     }
	public void add(Monom m) //metoda pentru adaugarea unui monom
	{
		if(exponent==m.getExponent())
			coeficient+=m.getCoeficient();
	}
	public static Monom inmultire(Monom m,Monom p){
		return new Monom(m.getCoeficient()*p.getCoeficient(),m.getExponent()+p.getExponent());
	}
}
