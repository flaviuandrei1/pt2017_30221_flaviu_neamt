import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Grafica extends JPanel {
	JButton adunare=new JButton("P1+P2");
	JButton scadere=new JButton("P1-P2");
	JButton inmultire=new JButton("P1*P2");
	JButton derivare=new JButton("P'");
	JButton integrare=new JButton("SP");
	JTextField p1=new JTextField(30);
	JTextField p2=new JTextField(30);
	JTextField rezultat=new JTextField(30);
	public Grafica(){ //constructor
		setLayout(new GridLayout(4,2)); //
		add(p1);
		add(adunare);
		add(p2);
		add(scadere);
		add(rezultat);
		add(inmultire);
		add(derivare);
		add(integrare);
		
		adunare.addMouseListener(new MouseAdapter(){ //aici punem event listener pentru adunare
			@Override
			public void mouseClicked(MouseEvent e){ //metoda se apeleaza de fiecare data cand este apasat butonul adunare
				//setam textul din rezultat sa fie suma polinoamelor din p1 si p2
				rezultat.setText(Polinom.adunarePolinoame(new Polinom(p1.getText()), new Polinom(p2.getText())).transformareInString());
			}
		});
		scadere.addMouseListener(new MouseAdapter(){ //aici punem event listener pentru adunare
			@Override
			public void mouseClicked(MouseEvent e){ //metoda se apeleaza de fiecare data cand este apasat butonul adunare
				//setam textul din rezultat sa fie suma polinoamelor din p1 si p2
				rezultat.setText(Polinom.scaderePolinoame(new Polinom(p1.getText()), new Polinom(p2.getText())).transformareInString());
			}
		});
		inmultire.addMouseListener(new MouseAdapter(){ //aici punem event listener pentru inmultire
			@Override
			public void mouseClicked(MouseEvent e){ //metoda se apeleaza de fiecare data cand este apasat butonul inmultire
				//setam textul din rezultat sa fie inmultirea polinoamelor din p1 si p2
				rezultat.setText(Polinom.inmultirePolinoame(new Polinom(p1.getText()), new Polinom(p2.getText())).transformareInString());
			}
		});
		derivare.addMouseListener(new MouseAdapter(){ //aici punem event listener pentru derivare
			@Override
			public void mouseClicked(MouseEvent e){ //metoda se apeleaza de fiecare data cand este apasat butonul derivare
				//setam textul din rezultat sa fie derivarea polinomului p1
				rezultat.setText(Polinom.derivarePolinom(new Polinom(p1.getText())).transformareInString());
			}
		});
		integrare.addMouseListener(new MouseAdapter(){ //aici punem event listener pentru integrare
			@Override
			public void mouseClicked(MouseEvent e){ //metoda se apeleaza de fiecare data cand este apasat butonul adunare
				//setam textul din rezultat sa fie integrarea prolinomului p1
				rezultat.setText(Polinom.integrarePolinom(new Polinom(p1.getText())).transformareInString());
			}
		});
	}
}
