import java.util.ArrayList;
import java.util.List;
public class Scheduler {
	private List<Server> servers;
	private int maxNoServers;
	
	public Scheduler(int maxNoServers,int maxTasksPerServer){
		this.maxNoServers = maxNoServers;
		servers=new ArrayList<Server>();
		for (int i=0; i<maxNoServers; i++){//creem servere 
			Server s = new Server(maxTasksPerServer);
			Thread t = new Thread(s);//creem un thread pentru fiecare server
			t.start();
			servers.add(s);//il punem in lista de servere
		}
	}
	public void dispatchTask(Task t){//pentru impartirea taskurilor
		int minim = 0;
		for (int i=0; i<maxNoServers; i++ ){
			if (servers.get(i).getWaitingTime() < servers.get(minim).getWaitingTime()) //gasim serverul cu cel mai mic timp de asteptare
				minim = i;
		}
		servers.get(minim).addTask(t); //adaugam taskul in serverul "minim"

		System.out.println("Task"+t.id+" la coada" + minim + "la timpul"+t.getArrivalTime());
	}
	public List<Server> getServers(){
		return servers;
	}
}
