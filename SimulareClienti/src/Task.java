
public class Task implements Comparable{
	public final int id;
	private int arrivalTime;
	private int waitingTime;
	private int processingPeriod;
	public Task(int arrivalTime,int processingTime, int id){
		this.arrivalTime=arrivalTime;
		this.processingPeriod=processingTime;
		this.id = id;
	}
	public int compareTo(Task anotherTask){
		return (new Integer(arrivalTime).compareTo(new Integer(anotherTask.arrivalTime)));
	}
	public String toString(){
		return processingPeriod +"";
	}
	public int getArrivalTime(){
		return arrivalTime;
	}
	public void setWaitingTime(int waitingTime){
		this.waitingTime=waitingTime;
	}
	public int getProcessingPeriod(){
		return processingPeriod;
	}
	public int getFinishTime(){
		return arrivalTime+processingPeriod+waitingTime;
	}
	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return arrivalTime - ((Task)arg0).getArrivalTime();
	}
}
