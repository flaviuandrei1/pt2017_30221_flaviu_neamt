import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimulationFrame extends JFrame {
	private static final long serialVersionUID=1L;
	//in panoul asta se pun intrarile si butonul
	private JPanel panelIntrari;
	//in asta se afiseaza cozile
	private JPanel panelAfisari;
	private int WIDTH=1000, HEIGHT=1000;
	//ca sa poti da start de la buton
	private SimulationManager sm;
	//texturi pentru valori de intrare
	JTextField nrClienti;
	JTextField timp;
	JTextField nrServere;
	JTextField minProcesare;
	JTextField maxProcesare;
	//buton de start #melestire
	JButton start;
	Thread t;
	public SimulationFrame(SimulationManager sm){
		this.sm = sm;
		this.setLayout(new GridLayout(2,0));
		panelIntrari=new JPanel(new GridLayout(6,2));
		panelAfisari=new JPanel();
		this.add(panelIntrari);
		this.add(panelAfisari);
		
		nrClienti = new JTextField();
		nrServere = new JTextField();
		timp = new JTextField();
		minProcesare = new JTextField();
		maxProcesare = new JTextField();
		start = new JButton("START");
		start.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sm.numberOfClients(Integer.parseInt(nrClienti.getText()));
				sm.maxProcessingTime(Integer.parseInt(maxProcesare.getText()));
				sm.minProcessingTime(Integer.parseInt(minProcesare.getText()));
				sm.timp(Integer.parseInt(timp.getText()));
				sm.numberOfServers(Integer.parseInt(nrServere.getText()));
				panelAfisari.setLayout(new GridLayout(1, Integer.parseInt(nrServere.getText())));
				t = new Thread(sm);
				t.start();
				
			}
		});
		panelIntrari.add(new JLabel("Nr clienti"));
		panelIntrari.add(nrClienti);
		panelIntrari.add(new JLabel("Nr servere"));
		panelIntrari.add(nrServere);
		panelIntrari.add(new JLabel("minim procesare"));
		panelIntrari.add( minProcesare);
		panelIntrari.add(new JLabel("maxim procesare"));
		panelIntrari.add(maxProcesare);
		panelIntrari.add(new JLabel("timp"));
		panelIntrari.add(timp);
		panelIntrari.add(new JLabel("MELESTIRE"));
		panelIntrari.add(start);
		this.setSize(WIDTH,HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
	}
	public void displayData(Task[][] tasks){
		panelAfisari.removeAll();
		panelAfisari.revalidate();
		for (int i = 0; i < tasks.length; i++){
			JTextArea tf = new JTextArea();
			JScrollPane sp = new JScrollPane(tf);
			panelAfisari.add(sp);
			tf.setText("Coada numarul "+(i+1));
			for (int j = 0; j<tasks[i].length;j++){
				tf.append("\nClient "+tasks[i][j].id+ " timp procesare:"+tasks[i][j].getProcessingPeriod());
			}
		}
		panelAfisari.revalidate();
	}
	
	

}
